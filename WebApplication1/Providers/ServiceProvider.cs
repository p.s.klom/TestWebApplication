﻿namespace WebApplication1.Providers
{
    public class ServiceProvider
    {
        private readonly string customProperty;

        public ServiceProvider(string customProperty)
        {
            this.customProperty = customProperty;
        }

        public string GetCustomPropertyValue()
        {
            return customProperty + ":Test";
        }
    }
}
