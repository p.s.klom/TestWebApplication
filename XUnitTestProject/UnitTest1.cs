namespace XUnitTestProject
{
    using WebApplication1.Providers;

    using Xunit;

    public class UnitTest1
    {
        [Theory]
        [InlineData("Jignesh")]
        [InlineData("Rakesh")]
        [InlineData("Not Found")]
        public void Test1(string data)
        {
            var serviceProvider = new ServiceProvider(data);
            string result = serviceProvider.GetCustomPropertyValue();
            Assert.Equal($"{data}:Test", result);
        }
    }
}